import Lambdas.AbstractCommand;
import Lambdas.Command;
import Lambdas.GetIntExecutor;
import Lambdas.MyCommand;
import Streams.RandList;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        //lambdas();
        streams();
    }

    private static void lambdas() {
        Command lambda1 = (a, b, c) -> a > b ? (a > c ? a : c) : (b > c ? b : c);
        Command lambda2 = (a, b, c) -> (a + b + c) / 3;
        System.out.println(lambda1.execute(1, 2, 3) + " " + lambda2.execute(1, 2, 3));

        //Command as lambda
        GetIntExecutor getIntExecutor = new GetIntExecutor();
        System.out.println(getIntExecutor.execute(lambda1, 1, 2, 3)
                + " " + getIntExecutor.execute(lambda2, 1, 2, 3));

        //Command as method reference
        MyCommand myCommand = new MyCommand();
        System.out.println(getIntExecutor.execute(myCommand::getMax, 1, 2, 3)
                + " " + getIntExecutor.execute(myCommand::getAverage, 1, 2, 3));

        //Command as anonymous class
        AbstractCommand command = new AbstractCommand() {
            @Override
            public Command getMax() {
                Command command = (a, b, c) -> a > b ? (a > c ? a : c) : (b > c ? b : c);
                return command;
            }

            @Override
            public Command getAverage() {
                Command command = (a, b, c) -> (a + b + c) / 3;
                return command;
            }
        };
        System.out.println(getIntExecutor.execute(command.getMax(), 1, 2, 3)
                + " " + getIntExecutor.execute(command.getMax(), 1, 2, 3));

        //Command as object of command
        System.out.println(getIntExecutor.execute(myCommand.getMax(), 1, 2, 3)
                + " " + getIntExecutor.execute(myCommand.getAverage(), 1, 2, 3));
    }

    private static void streams() {
        List<Integer> list = new RandList(10).getList();
        int average = list.stream().reduce((e1, e2) -> e1 + e2).get() / list.size();
        int min = list.stream().min(Integer::compareTo).get();
        int max = list.stream().max(Integer::compareTo).get();
        int sumWithReduce = list.stream().reduce((e1, e2) -> e1 + e2).get();
        int sumWithSum = list.stream().mapToInt(Integer::intValue).sum();
        int biggerThanAvgNumber = (int) list.stream().filter(i -> i > average).count();

        System.out.println("Your list:");
        for (Integer el : list)
            System.out.print(el + " ");
        System.out.println("\nAverage = " + average);
        System.out.println("Min = " + min);
        System.out.println("Max = " + max);
        System.out.println("Sum using .reduce = " + sumWithReduce);
        System.out.println("Sum using .sum = " + sumWithSum);
        System.out.println("Number of values that are bigger than average = " + biggerThanAvgNumber);
    }
}
