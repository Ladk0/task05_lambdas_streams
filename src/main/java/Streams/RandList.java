package Streams;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RandList {
    private List<Integer> list;

    public RandList(int size) {
        list = IntStream.generate(() -> ThreadLocalRandom.current().nextInt(10)).limit(size).boxed().collect(Collectors.toList());
    }

    public List<Integer> getList() {
        return list;
    }
}
