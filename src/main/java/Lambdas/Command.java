package Lambdas;

public interface Command {
    int execute(int a, int b, int c);
}
