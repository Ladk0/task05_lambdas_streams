package Lambdas;

public class MyCommand extends AbstractCommand{
    public int getMax(int a, int b, int c){
        return a > b ? (a > c ? a : c) : (b > c ? b : c);
    }
    public int getAverage(int a, int b, int c){
        return (a + b + c) / 3;
    }
}
