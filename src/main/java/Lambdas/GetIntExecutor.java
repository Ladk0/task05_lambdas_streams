package Lambdas;

import java.util.ArrayList;
import java.util.List;

public class GetIntExecutor {
    private final List<Command> commands = new ArrayList<>();

    public int execute(Command command, int a, int b, int c) {
        commands.add(command);
        return command.execute(a, b, c);
    }
}
