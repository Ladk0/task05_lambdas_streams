package Lambdas;

public abstract class AbstractCommand {
    public Command getMax(){
        Command command = (a, b, c) -> a > b ? (a > c ? a : c) : (b > c ? b : c);
        return command;
    }
    public Command getAverage(){
        Command command = (a, b, c) -> (a + b + c)/3;
        return command;
    }
}
